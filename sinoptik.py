# -*- coding: utf-8 -*-
# !/usr/bin/python3.5

from requests import get
from bs4 import BeautifulSoup as Bts
from datetime import date


class Weather(object):

    def __init__(self):
        self.var = ('', ' мм', ' %', 'м/сек', ' %')
        self.URL = "https://ua.sinoptik.ua/"
        self.is_future = True
        self.DATE = str(date.today())
        self.region = 'львівська-область/'
        self.district = 'жовківський-район/'
        self.parsing_type = 'lxml'
        self.dt = date

    def set_region(self, region: str):
        #   region:
        #   область
        self.region = '%s/' % region.lower()

    def set_district(self, district: str):
        # district
        #   район
        self.district = '%s/' % district.lower()

    def set_date(self, d):
        self.DATE = d

    def update(self, path: str):
        current_path = 'https://%s/%s' % (path, self.DATE)

        try:
            self.response = get(current_path).text
        except Exception as er:
            print("Exception: ", er)
            exit(0)

        page = Bts(self.response, self.parsing_type)
        main = page.find('div', class_="main loaded")

        self.is_future = self.DATE >= str(date.today())

        self.TITLE = current_path

        self.MIN_TEMP = main.find(class_='min').text.split()[-1]

        self.MAX_TEMP = main.find(class_='max').text.split()[-1]

        self.WEATHER = main.find(
            'div', class_="weatherIco").get("title")

        self.WEATHER_DETAILS = page.find(
            'table',
            class_='weatherDetails').find('tbody').find_all('tr')

        self.INFO = page.find(
            'div',
            class_="description").text

    def get_list_region(self) -> dict:
        try:
            self.response = get('%sукраїна/' % self.URL).text
        except Exception as er:
            print(er)
            exit(0)

        page = Bts(self.response, self.parsing_type)

        main = page.find(
            class_='mapRightCol').find_all('ul')[1].find_all('a')

        del page

        return {context.text: context.get('href') for context in main}

    def get_list_district(self) -> dict:
        response = get('%sукраїна/%s' %
                       (self.URL, self.region)).text
        page = Bts(response, self.parsing_type)

        main = page.find(
            class_='mapRightCol').find_all('ul')[1].find_all('a')

        del page

        return {context.text: context.get('href') for context in main}

    def get_list_position(self) -> dict:
        try:
            self.response = get('%sукраїна/%s/%s' %
                                (self.URL, self.region,
                                 self.district)
                                ).text
        except Exception as er:
            print("Exception: ", er)
            exit(0)

        page = Bts(self.response, self.parsing_type)

        data = Bts(str(page.find('div', class_='mapBotCol')
                       .find_all('div', class_='clearfix')[0]
                       .find_all('ul', class_='col6')), self.parsing_type) \
            .find_all("li")

        del page

        return {context.text[1:-1]: context.find('a').get('href')[2:]
                for context in data}

    def get_title(self) -> str:
        return str(self.TITLE)

    def get_min_temp(self) -> str:
        return self.MIN_TEMP

    def get_max_temp(self) -> str:
        return self.MAX_TEMP

    def get_weather(self) -> str:
        return str(self.WEATHER)

    def get_info(self) -> str:
        return str(self.INFO)

    def get_time_line(self) -> list:
        return [x.text.replace(' ', '')
                for x in self.WEATHER_DETAILS[0].find_all('td')]

    def get_more_weather_info(self) -> dict:
        weather_var_code = (2, 4, 5, 6, 7) \
            if self.is_future else (2, 3, 4, 5)

        self.var_name = ('температура: ',
                         'тиск: ',
                         'вологість: ',
                         'швидкість вітру: ',
                         'ймовірність опадів: ')

        self.weather_var = [[x.text for j, x in
                             enumerate(Bts(str(self.WEATHER_DETAILS[i]),
                                           self.parsing_type).find_all('td'))]
                            for i in weather_var_code]

        self.line_ = {
            x: ['%s%s%s' % (self.var_name[i], self.weather_var[i][j], self.var[i]) for i in
                range(len(weather_var_code))]
            for j, x in enumerate(self.get_time_line())}
        result = self.line_

        return result

    def show_tables(self):
        from pandas import DataFrame

        self.get_more_weather_info()

        data = [[y + self.var[i] for y in x]
                for i, x in enumerate(self.weather_var)]

        print(DataFrame(
            data=data,
            dtype=str,
            index=self.var_name if self.is_future else self.var_name[:-1],
            columns=self.get_time_line())
        )
