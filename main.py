# -*- coding: utf-8 -*-
# !/usr/bin/python3.5


from gc import collect
from datetime import date
from datetime import timedelta
from json import loads
from notify2 import Notification
from notify2 import init
from requests import get
from sinoptik import Weather
from time import sleep
from sys import argv


def notify():
    for i in range(2):
        s = Weather()
        s.set_district('Львів')
        s.set_date((date.today() + timedelta(days=i)).isoformat())
        s.update(s.get_list_position()['Львів'])
        init("Cryptocurrency rates notifier")
        n = Notification("Crypto Notifier")
        n.update(s.get_title(), "%s\n%s\n%s" %
                 (s.get_weather(),
                  'мін. температура: %s\nмакс. температура: %s' %
                  (s.get_min_temp(), s.get_max_temp()), s.get_info()))
        n.show()


def settings() -> dict:
    path = argv[0]
    with open("%s/settings" % path[:path.rfind('/')], 'r') as file:
        d = ''
        for i in file:
            d += i
        data = loads(d)
    return data


def show():
    while True:
        try:
            get('http://www.google.com.ua')
        except:
            print('No internet connection!')
            sleep(5)
        else:
            print('Internet connection!')
            notify()
            try:
                collect()
                sleep(settings().get('delay'))
            except KeyboardInterrupt:
                print('>>\nClose.')
                exit(0)


if __name__ == '__main__':
    show()
